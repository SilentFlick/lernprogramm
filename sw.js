const cacheName = 'Lernprogramm-v1';
const assets = [
  '/index.html',
  '/styles/main.css',
  '/scripts/parseJson.js',
  '/questions/questions.json',
  '/images/icon-64x64.png',
  '/images/icon-128x128.png',
  '/images/icon-256x256.png'
];

// Cache all the files to make a PWA
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll(assets);
    })
          .then(() => {
            return self.skipWaiting();
          })
  );
});

self.addEventListener('fetch', async e => {
  const req = e.request;
  const url = new URL(req.url);
  if(url.origin === location.origin) {
    e.respondWith(cacheFirst(req));
  } else {
    e.respondWith(networkCache(req));
  }
});

async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cached = await cache.match(req);
  return cached || fetch(req);
}

async function networkCache(req) {
  const cache = await caches.open(cacheName);
  try {
    const fresh = await fetch(req);
    await cache.put(req, fresh.clone());
    return fresh;
  } catch(e) {
    const cached = await cache.match(req);
    return cached;
  }
}
