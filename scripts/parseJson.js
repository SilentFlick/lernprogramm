"use strict";

var currentQuestion = 0;
var numberOfQuestions = 0;
var numberOfAnswer = 4;
var numberOfRightAnswer = 0;
var currentSubject;
var usedNumber = new Set();

function parseJson() {
  var xhttp = new XMLHttpRequest();
  var i, currentAnswer;
  var buttonsLabel = [], buttonsValue = [];
  var jsonQuestion = "a";
  var jsonAnswer = "l";
  var katexDisplay = "";
  var katexInline = "";
  if(currentSubject === "mathe") {
    katexDisplay = "$$";
    katexInline = "$";
  }
  if(currentSubject === "content") {
    xhttp.open("GET", "https://irene.informatik.htw-dresden.de:8888/api/quizzes/", true);
    jsonQuestion = "text";
    jsonAnswer = "options";
  } else {
    xhttp.open("GET", "questions/questions.json", true);
  }
  xhttp.onreadystatechange = function() {
    if(this.readyState === XMLHttpRequest.DONE) {
      var status = this.status;
      if(status === 0 || (status >= 200 && status < 400)) {
        var obj = JSON.parse(xhttp.responseText);
        numberOfQuestions = Object.keys(obj[currentSubject]).length;
        if(currentQuestion < numberOfQuestions) {
          var tmp = currentQuestion + 1;
          document.getElementById("A-Label-" + currentSubject).innerHTML = "Aufgabe " + tmp + "/" + numberOfQuestions + ":";
          document.getElementById("Q-" + currentSubject).innerHTML =  katexDisplay + obj[currentSubject][currentQuestion][jsonQuestion] + katexDisplay;
          document.getElementById("L-Label-" + currentSubject).innerHTML = "Lösung: ";
          for(i = 0; i < numberOfAnswer; i++) {
            currentAnswer = randomIntFromInterval() - 1;
            buttonsLabel[i]=  document.getElementById("L-Button-Label-" + i + "-" + currentSubject);
            buttonsLabel[i].textContent = katexInline + obj[currentSubject][currentQuestion][jsonAnswer][currentAnswer] + katexInline;
            buttonsLabel[i].style.backgroundColor = '';
            buttonsValue[i] = document.getElementById("LB" + i + "-" + currentSubject);
            buttonsValue[i].checked = false;
            buttonsValue[i].value = currentAnswer;
          }
          renderMathInElement(document.body, {
            // customised options
            // • auto-render specific keys, e.g.:
            delimiters: [
              {left: '$$', right: '$$', display: true},
              {left: '$', right: '$', display: false},
              {left: '\\(', right: '\\)', display: false},
              {left: '\\[', right: '\\]', display: true}
            ],
            // • rendering keys, e.g.:
            throwOnError : false
          });
          currentQuestion++;
          usedNumber.clear();
          console.log("Parse JSON successfully");
        }
      } else {
        console.log(status);
      }
    }
  }
  if(currentSubject === "content") {
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Authorization", "Basic " + window.btoa("s82053@gmail.com:abd16hg"));
    xhttp.send();
  } else {
    xhttp.send();
  }
}

function openTab(evt, subject) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
  }

  var progress = document.getElementsByClassName("tablinks active");
  if(progress.length != 0){
    //window.alert("Warning: your progress will be lost!");
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(subject).style.display = "block";
  evt.currentTarget.className += " active";
  currentSubject = subject;
  currentQuestion = 0;
  numberOfQuestions = 0;
  numberOfRightAnswer = 0;
  document.getElementById("score").innerHTML = "Score: " + numberOfRightAnswer;
  document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
  document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'hidden';
}

function getAnswer() {
  var i;
  var button = [];
  for(i = 0; i < numberOfAnswer; i++) {
    button[i] = document.getElementById("LB" + i + "-" + currentSubject);
    if(button[i].value == "0") {
      document.getElementById("L-Button-Label-" + i + "-" + currentSubject).style.backgroundColor = 'green';
      if(button[i].checked) {
        numberOfRightAnswer++;
        document.getElementById("score").innerHTML = "Score: " + numberOfRightAnswer;
      }
    } else {
      document.getElementById("L-Button-Label-" + i + "-" + currentSubject).style.backgroundColor = 'red';
    }
  }
  document.getElementById("Next-Button-" + currentSubject).style.visibility = 'visible';
  if(currentQuestion == numberOfQuestions) {
    document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'visible';
    document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
  }
}

function nextQuestion() {
  document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
  document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'hidden';
  parseJson();
}

function resetQuestion() {
  document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
  document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'hidden';
  currentQuestion = 0;
  numberOfQuestions = 0;
  numberOfRightAnswer = 0;
  parseJson();
}
function randomIntFromInterval(min = 1, max = 4) { // min and max included
  let newNumber;
  do{
    if(usedNumber.size === max) {
      throw new Error('No more uniques!');
    }
    newNumber = Math.floor(Math.random() * (max - min + 1) + min);
  } while(usedNumber.has(newNumber));
  usedNumber.add(newNumber);
  return newNumber;
}

function startTime() {
  const today = new Date();
  let h = today.getHours();
  let m = today.getMinutes();
  let s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('clock').innerHTML =  h + ":" + m + ":" + s;
  setTimeout(startTime, 1000);
}

function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
