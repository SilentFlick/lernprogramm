# Inhaltsverzeichnis

1.  [Einführung](#org68ccac2)
2.  [Kategorien Auswahl](#org345c247)
3.  [Laden der Fragen und Antworten](#org390a7fc)
4.  [Erweiterung des Lernprogramms](#org08e39d2)
5.  [Lernportfolio](#orgc2a0412)



<a id="org68ccac2"></a>

# Einführung

Dieses Dokument dient als Lernportfolio und Beschreibung der Funktionalität von spezifischen Quellcode-Komponenten. Ziel ist es, dass das Lernprogramm besiztz:

-   Wahl zwischen 4 verschiedenen Aufgabenkategorien: Mathematik, Internettechnologien, Allgemein und Random (Rest-API).
-   Jede Frage sollte 4 Antworten, die in zufälliger Reihenfolge sind.
-   Nach Beantwortung der Frage soll die richtige Antwort mit den falschen per Hintergrundfarbe (rot und grün) angezeigt werden.
-   Verwendung von KaTex zum Rendern von grafischen Formeldarstellung.
-   Die Anzeige sollte sich an verschiedene Geräte anpassen
-   Die Möglichkeit, die Fragen dynamisch hinzufügen oder zu entfernen.

Das Lernprogramm findet unter [Lernprogramm](http://www.informatik.htw-dresden.de/~s82053/Lernprogramm/) und die Quellcode unter [Github](https://github.com/HTWDD-RN/s82053-it1-beleg/tree/beleg). Und es gibt zwei Hauptfunktionen openTab() und parseJson().


<a id="org345c247"></a>

# Kategorien Auswahl

Die Funktion openTab() blendet zunächst alle Klasse &bdquo;tabcontent&ldquo; aus.

    // Declare all variables
    var i, tabcontent, tablinks;
    
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

Das zweite Argument dieser Funktion bestimmt, welche Kategorienskarte angezeigt wird und auch das aktuelle Thema, das für die Funtion parseJson() wichtig ist.

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(subject).style.display = "block";
    evt.currentTarget.className += " active";

Wenn der Benutzer das Thema wechselt, sollten alle globalen Variablen zurückgesetzt werden.

    currentSubject = subject;
    currentQuestion = 0;
    numberOfQuestions = 0;
    numberOfRightAnswer = 0;
    document.getElementById("score").innerHTML = "Score: " + numberOfRightAnswer;
    document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
    document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'hidden';

Aus dem obigen Codeausschnitt, wenn eine Registerkarte angezeigt wird, wird das Wort &bdquo;active&ldquo; zu ihrem Name hinzufügt. Auf diese Weise können wir die Funktionalität des Lernprogramms erweitern, indem wir zum Beispiel den aktuellen Fortschritt vor einem Themenwechsel speichern.
Das bedeutet auch, dass wir dieses Wort entfernen müssen, bevor wir eine Karte anzeigen.

    var progress = document.getElementsByClassName("tablinks active");
    if(progress.length != 0){
      //window.alert("Warning: your progress will be lost!");
      //saveCurrentProgress();
    }
    
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }


<a id="org390a7fc"></a>

# Laden der Fragen und Antworten

Die Funktion openTab() setzt den Wert der Variable currentSubject, und wir verwenden diesen Wert, und zu bestimmen, ob die Fragen und Antworten von KaTex gerendert werden sollen und ob REST verwendet werden soll.

    var jsonQuestion = "a";
    var jsonAnswer = "l";
    var katexDisplay = "";
    var katexInline = "";
    if(currentSubject === "mathe") {
      katexDisplay = "$$";
      katexInline = "$";
    }
    if(currentSubject === "content") {
      xhttp.open("GET", "https://irene.informatik.htw-dresden.de:8888/api/quizzes/", true);
      jsonQuestion = "text";
      jsonAnswer = "options";
    } else {
      xhttp.open("GET", "questions/questions.json", true);
    }

Um zu vermeiden, dass HTML in das falsche div eingefügt wird, wird das currentSubject in der div-ID verwendet.
Und Object.keys(obj[currentSubject]).length wird verwendet, um die Gesamtzahl der Fragen zu ermitteln.
Die Daten werden im Cache gespeichert und wenn sich nichts ändert, holt sich der Client die nächste Frage aus dem Cache.

    xhttp.onreadystatechange = function() {
      if(this.readyState === XMLHttpRequest.DONE) {
        var status = this.status;
        if(status === 0 || (status >= 200 && status < 400)) {
          var obj = JSON.parse(xhttp.responseText);
          numberOfQuestions = Object.keys(obj[currentSubject]).length;
          if(currentQuestion < numberOfQuestions) {
            var tmp = currentQuestion + 1;
            document.getElementById("A-Label-" + currentSubject).innerHTML = "Aufgabe " + tmp + "/" + numberOfQuestions + ":";
            document.getElementById("Q-" + currentSubject).innerHTML =  katexDisplay + obj[currentSubject][currentQuestion][jsonQuestion] + katexDisplay;
            document.getElementById("L-Label-" + currentSubject).innerHTML = "Lösung: ";
            for(i = 0; i < numberOfAnswer; i++) {
              currentAnswer = randomIntFromInterval() - 1;
              buttonsLabel[i]=  document.getElementById("L-Button-Label-" + i + "-" + currentSubject);
              buttonsLabel[i].textContent = katexInline + obj[currentSubject][currentQuestion][jsonAnswer][currentAnswer] + katexInline;
              buttonsLabel[i].style.backgroundColor = '';
              buttonsValue[i] = document.getElementById("LB" + i + "-" + currentSubject);
              buttonsValue[i].checked = false;
              buttonsValue[i].value = currentAnswer;
            }
            renderMathInElement(document.body, {
              // customised options
              // • auto-render specific keys, e.g.:
              delimiters: [
                {left: '$$', right: '$$', display: true},
                {left: '$', right: '$', display: false},
                {left: '\\(', right: '\\)', display: false},
                {left: '\\[', right: '\\]', display: true}
              ],
              // • rendering keys, e.g.:
              throwOnError : false
            });
            currentQuestion++;
            usedNumber.clear();
            console.log("Parse JSON successfully");
          }
        } else {
          console.log(status);
        }
      }
    }

Und die gleiche Technik kann wieder verwendet werden, wenn die Anfrage gesendet wird.

    if(currentSubject === "content") {
      xhttp.setRequestHeader("Content-Type", "application/json");
      xhttp.setRequestHeader("Authorization", "Basic " + window.btoa("s82053@gmail.com:abd16hg"));
      xhttp.send();
    } else {
      xhttp.send();
    }

Der folgende Codeausschnitt gibt eine zufällige, nicht duplizierte Zahl zurück. Dies ermöglicht es die Funktion parseJson(), die Antworten zufällig anzuzeigen und kann auch auf die Fragen ausgedehnt werden.
Vom Bereich min bis max, die kleinste Zahl ist der Wert der richtigen Antwort.

    function randomIntFromInterval(min = 1, max = 4) { // min and max included
      let newNumber;
      do{
        if(usedNumber.size === max) {
          throw new Error('No more uniques!');
        }
        newNumber = Math.floor(Math.random() * (max - min + 1) + min);
      } while(usedNumber.has(newNumber));
      usedNumber.add(newNumber);
      return newNumber;
    }

Nachdem der Benutzer eine Frage beantwortet hat, ändert das Programm die Hintergrundfarbe aller Antworten.
Die Antwort mit dem kleinsten Wert ist die richtige und hat die Hintergrundfarbe grün, die anderen Antworten rot.
Wenn der Benutzer die richtige Antwort auswählt, wird der Wert der Variablen numberOfRightAnswer um 1 erhöht,

    function getAnswer() {
      var i;
      var button = [];
      for(i = 0; i < numberOfAnswer; i++) {
        button[i] = document.getElementById("LB" + i + "-" + currentSubject);
        if(button[i].value == "0") {
          document.getElementById("L-Button-Label-" + i + "-" + currentSubject).style.backgroundColor = 'green';
          if(button[i].checked) {
            numberOfRightAnswer++;
            document.getElementById("score").innerHTML = "Score: " + numberOfRightAnswer;
          }
        } else {
          document.getElementById("L-Button-Label-" + i + "-" + currentSubject).style.backgroundColor = 'red';
        }
      }
      document.getElementById("Next-Button-" + currentSubject).style.visibility = 'visible';
      if(currentQuestion == numberOfQuestions) {
        document.getElementById("Reset-Button-" + currentSubject).style.visibility = 'visible';
        document.getElementById("Next-Button-" + currentSubject).style.visibility = 'hidden';
      }
    }


<a id="org08e39d2"></a>

# Erweiterung des Lernprogramms

-   Speichern den aktuellen Fortschritt vor dem Wechsel der Kategorie.
-   Die Reihenfolge der Fragen sollte zufällig sein.
-   Die Datei questions.json sollte zwischengespeichert werden, wenn der Benutzer das Programm öffnet, d.h. bevor er auf die Schaltfläche &bdquo;Tab&ldquo; (openTab) klickt.
-   PWA funktioniert nicht korrekt.


<a id="orgc2a0412"></a>

# Lernportfolio

Die Idee ist, ich gruppiere eine Menge von HTML-Tag auf der Grundlage der Kategorie und verwenden auch die gleiche Tag-ID, so dass, wenn ich document.getElementById aufrufen, kann ich den Wert des HTML-Tag mit einer Schleife ändern.
Deswegen kann ich für alle Gruppe denselben HTML-Code verwenden und nur die ID (Subject) ändern.
Das heißt, wenn ich ein neues Thema hinzufügen möchte, kann ich diesen Codeschnipsel wiederverwenden und der Datei questions.json neue Fragen hinzufügen.
Wie ich bereits erklärt habe, braucht das Programm eine einzige Funktion, um die Fragen auf verschiedene Arten zu erhalten (lokal oder REST) und zu bestimmen, ob die und Antworten gerendert werden sollen.
Idealerweise kann die gewählte Antwort lokal überprüft werden, ob sie richtig oder falsch ist.

Ich lasse die Zahlen von 1 bis 4 mit der Funktion randomIntFromInterval() zufällig generieren.
Und weil ich die richtige Antwort schon kenne, füge ich beim  Einfügen der Antworten in die HTML-Labels auch diese Zahl ein (innerHTML).
Wenn die Funktion getAnswer() aufgerufen wird, muss diese Funktion nur die Zahl, d.h. value, überprüfen und die richtigen und falschen Antworten anzeigen.

Wenn ich das Lernprogramm mit vielen Webbrowsern gestestet habe, funktioniert PWA nict immer auf die gleiche Weise.
Firefox und LibreWolf (Flatpak) blockieren den Speicherzugriff und das bedeutet, dass PWA nicht funktioniert,
qutebrowser zeigt keine Warnung oder Fehler in Application (Console) aber Chromium zeigt einen.
